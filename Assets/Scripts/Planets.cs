﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planets : MonoBehaviour
{
    public float OrbitSpeed;
    public GameObject point;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0,OrbitSpeed,0));
        gameObject.transform.position = point.transform.position;
    }
}
